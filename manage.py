#! /usr/bin/env python2.7

from contextlib import closing
import csv
import hashlib
import logging
import os
from urllib2 import urlopen, URLError, HTTPError
from urlparse import urlsplit
import argparse

log = logging.getLogger( os.path.basename( __file__ ) if __name__ == '__main__' else __name__ )


def import_repository( dir_path ):
    urls = set( )
    paths = set( )
    for center_name in os.listdir( dir_path ):
        if center_name.startswith( '.' ): continue
        center_path = os.path.join( dir_path, center_name )
        if not os.path.isdir( center_path ): continue
        tsv_path = os.path.join( center_path, 'tcga.tsv' )
        if not os.path.isfile( tsv_path ):
            log.warn( 'No TSV found for %s', center_name )
            continue
        with open( tsv_path ) as tsv_file:
            rows = csv.DictReader( tsv_file, delimiter='\t', strict=True )
            for i, row in enumerate( rows ):
                for file_column_name in ( 'target_file', 'probe_file' ):
                    path_or_url = row[ file_column_name ]
                    if path_or_url.startswith( 'vendor/' ):
                        path = os.path.join( center_path, path_or_url )
                        paths.add( path )
                    else:
                        url = urlsplit( path_or_url )
                        # We can't use urlparse.urldefrag() because it has this stupid
                        # 'optimization' of returning the argument unmodified if # is not present
                        #  in the URL. This means that we only get the normalizing side-effects
                        # of urlparse/urlsplit if the url contains a #. The side-effects are
                        # lowercasing the scheme and dropping empty queries and fragments.

                        # FIXME: It would be nice if we normalized the URL (paths with .. in
                        # them, + vs %20 ambiguity etc.) such that those ambiguities don't cause
                        # redundant cache entries.
                        if url.fragment:
                            url = url._replace( fragment='' )
                        urls.add( url.geturl( ) )
    return urls, paths


def check_files( paths ):
    errors = 0
    for path in paths:
        if not os.path.isfile( path ):
            log.error( '%s does not exist', path )
            errors += 1
    return errors == 0


def cache_urls( dir_path, urls ):
    errors = 0
    cache_dir_path = os.path.join( dir_path, 'cache' )
    if not os.path.exists( cache_dir_path ):
        os.mkdir( cache_dir_path )
    for url in urls:
        if not url: continue
        url_hash = hashlib.sha1( url ).hexdigest( )
        cache_file_path = os.path.join( cache_dir_path, url_hash )
        if not os.path.isfile( cache_file_path ):
            log.info( 'Cache miss %s (%s)', url_hash, url )
            tmp_cache_path = cache_file_path + '.tmp'
            with open( tmp_cache_path, mode='wb' ) as output_file:
                try:
                    with closing( urlopen( url ) ) as input_stream:
                        while True:
                            buf = input_stream.read( 1024 * 1024 )
                            if not buf:
                                break
                            output_file.write( buf )
                except HTTPError as e:
                    log.exception( 'Cannot download %s, http status %s', e.geturl( ), e.getcode( ) )
                    errors += 1
                except URLError:
                    log.exception( 'Cannot download %s', url )
                    errors += 1
            os.rename( tmp_cache_path, cache_file_path )
        else:
            log.info( 'Cache hit %s (%s)', url_hash, url )
    return errors == 0


def main( ):
    logging.basicConfig( )
    dir_path = os.getcwd( )
    parser = argparse.ArgumentParser( formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    levels = [ logging.CRITICAL, logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG ]
    parser.add_argument( '--verbosity',
                         help='Set the level of detail for the console output',
                         default=logging.getLevelName( logging.INFO ),
                         choices=[ logging.getLevelName( l ) for l in levels ] )
    group = parser.add_mutually_exclusive_group( required=True )
    group.add_argument( '--cache-urls', action='store_true',
                        help='Cache URLs to .bed files locally' )
    group.add_argument( '--check-files', action='store_true',
                        help='Check the presence of any local files mentioned in the .tsv files' )
    options = parser.parse_args( )
    logging.root.setLevel( logging.getLevelName( options.verbosity ) )
    urls, paths = import_repository( dir_path )
    if options.cache_urls:
        if not cache_urls( dir_path, urls ):
            raise ( RuntimeError( "Some urls could not be cached" ) )
    elif options.check_files:
        if not check_files( paths ):
            raise ( RuntimeError( "Some files could not be checked" ) )


if __name__ == '__main__':
    main( )
